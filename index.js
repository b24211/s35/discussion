const express = require("express");
// Import mongoose package
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
/*
	Syntax:
		mongoose.connect("<MongoDB atlas connection string>", 
			{ 
				useNewUrlParser : true 
			}
		)
*/
mongoose.connect("mongodb+srv://Rahul_Sriharsha:dzi6Gyo3QhtrYacc@zuitt-bootcamp.y41ihuq.mongodb.net/b242_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// Connection to the database
let db = mongoose.connection;

// if a connection error occured, output will be in the console
db.on("error", console.error.bind(console, "connection error"));

// if the connection is successful
db.once("open", () => console.log("We're connected to the cloud database"));

// Mongoose schemas
// Creates a new task schema
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
});

// Task model
const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create new task
app.post("/tasks", (req, res) => {
	Task.findOne({name : req.body.name}, (err, result) => {
		if(result != null && result.name === req.body.name){
			return res.send("Duplicate task found");
		}
		else{
			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

// GET request to retrieve all the tasks

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

// s35 Activity

const userSchema = new mongoose.Schema({
	username : String,
	password : {
		type : String,
		default : "pending"
	}
})

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
	Task.findOne({username : req.body.username}, (err, result) => {
		if(result != null && result.username === req.body.username){
			return res.send("Duplicate user found");
		}
		else{
			let newTask = new Task({
				username : req.body.username
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New User created");	
				}
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`));
